import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class WiFi_Config extends StatefulWidget {
  @override
  State<WiFi_Config> createState() => _WiFi_ConfigState();
}

class _WiFi_ConfigState extends State<WiFi_Config> {
  TextEditingController _SSIDcontroller = new TextEditingController();
  bool ssid_text = false;
  bool pass_text = false;
  TextEditingController _Passwordcontroller = new TextEditingController();

  Future<http.Response> PostRequest(var SSID, var Password) {
    var data = {'status': SSID, 'id': Password};
    return http.post(
      Uri.parse('http://192.168.4.1/setting?ssid=${SSID}22&pass=$Password'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      // http://192.168.4.1/setting?ssid=Raghavendiran22&pass=wwe
      body: jsonEncode(data),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    //connect();
    super.initState();
  }

  String? get _ssidValue {
    // at any time, we can get the text from _controller.value.text
    final SSID = _SSIDcontroller.value.text;
    // Note: you can do your own custom validation here
    // Move this logic this outside the widget for more testable code
    if (SSID.isEmpty) {
      return 'Can\'t be empty';
    }
    if (SSID.length < 4) {
      return 'Too short';
    }
    if (RegExp(r'/^[A-Za-z0-9_@./#&+-]*$/').hasMatch(_SSIDcontroller.text)) {
      return 'No space is Acceptable';
    }
    // return null if the text is valid
    return null;
  }

  String? get _passValue {
    // at any time, we can get the text from _controller.value.text
    final Pass = _Passwordcontroller.value.text;
    // Note: you can do your own custom validation here
    // Move this logic this outside the widget for more testable code
    if (Pass.isEmpty) {
      return 'Can\'t be empty';
    }
    if (Pass.length < 4) {
      return 'Too short';
    }
    if (RegExp(r'/^[A-Za-z0-9_@./#&+-]*$/').hasMatch(_SSIDcontroller.text)) {
      return 'No space is Acceptable';
    }
    // return null if the text is valid
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blueAccent),
            borderRadius: BorderRadius.all(Radius.circular(6.0)),
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color(0xFF5258C9),
                Color(0xFFC5C7EA),
              ],
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 40,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("WiFi SSID : "),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _SSIDcontroller,
                  //RegExp(/^[A-Za-z0-9_@./#&+-]*$/),
                  // validator: (_ssidValue) {
                  //   if (_SSIDcontroller.text.isEmpty ||
                  //       !RegExp(r'/^[A-Za-z0-9_@./#&+-]*$/')
                  //           .hasMatch(_SSIDcontroller.text)) {
                  //     return;
                  //   } else {
                  //     print("error");
                  //   }
                  // },
                  decoration: InputDecoration(
                    //errorText: _ssidValue,
                    hintText: "SSID",
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.white),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text("Password : "),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: _Passwordcontroller,
                  decoration: InputDecoration(
                      //errorText: _passValue,
                      hintText: "Password",
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(width: 1, color: Colors.white))),
                ),
                SizedBox(
                  height: 20,
                ),
                TextButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.white),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue),
                    overlayColor: MaterialStateProperty.resolveWith<Color?>(
                      (Set<MaterialState> states) {
                        if (states.contains(MaterialState.hovered))
                          return Colors.blue.withOpacity(0.04);
                        if (states.contains(MaterialState.focused) ||
                            states.contains(MaterialState.pressed))
                          return Colors.blue.withOpacity(0.12);
                        return null; // Defer to the widget's default.
                      },
                    ),
                  ),
                  onPressed: () {
                    PostRequest(_SSIDcontroller.text, _Passwordcontroller.text);
                    Navigator.pop(context);
                  },
                  child: SizedBox(
                    height: 30,
                    width: 100,
                    child: Align(
                        alignment: Alignment.center, child: Text('Connect')),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                TextButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.white),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue),
                    overlayColor: MaterialStateProperty.resolveWith<Color?>(
                      (Set<MaterialState> states) {
                        if (states.contains(MaterialState.hovered))
                          return Colors.blue.withOpacity(0.04);
                        if (states.contains(MaterialState.focused) ||
                            states.contains(MaterialState.pressed))
                          return Colors.blue.withOpacity(0.12);
                        return null; // Defer to the widget's default.
                      },
                    ),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    //_dbref.child("LivingRoom/WiFi").set({'rest_status': 0});
                    //PostRequest(_SSIDcontroller.text, _Passwordcontroller.text);
                  },
                  child: SizedBox(
                    height: 30,
                    width: 100,
                    child: Align(
                        alignment: Alignment.center, child: Text('Cancel')),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
