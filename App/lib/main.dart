import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:home_automation/login/sign_up.dart';
import 'package:home_automation/screens/homeScreenBle.dart';
import 'package:provider/provider.dart';

import 'login/google_sign_in.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) => ChangeNotifierProvider(
      create: (context) => GoogleSignInProvider(),
      child: MaterialApp(
        title: 'Smart-Home',
        initialRoute: "/",
        debugShowCheckedModeBanner: false,
        routes: {
          "/": (context) => Signup_Page(),
          "home": (context) => HomeScreenBluetooth(),
        },
      ));
}
// @override
// Widget build(BuildContext context) => ChangeNotifierProvider(
//       create: (context) => GoogleSignInProvider(),
//       child: MaterialApp(
//         debugShowCheckedModeBanner: false,
//         title: "MainPage",
//         theme: ThemeData.dark(),
//         // home: Signup_Page(),
//         home: HomeScreenBluetooth(),
//         //home: HomeScreen(),
//         //home: RealTimeDB(),
//         // home: PreviousState(),
//       ),
//     );
